def _orjoin(d , functions):
    result = False
    for fun in functions:
        result = result or fun(d)
    return result

def _andjoin(d , functions):
    result = True
    for fun in functions:
        result = result and fun(d)
    return result

def _notoper(d , functions):
    f = functions[-1]
    return not(f(d))

def simpleparse(d , string , orOper = _orjoin , andOper = _andjoin , notOper = _notoper):
    """
    Parse a string.
    d : [[<compiled regular expression> , <function of two arguments - the expression to be checked againsst and the string in the expression>] , ...] 
    string : str with expression
    """
    funlist = string.split()
    funlambda = []
    for fun in funlist:
        ok = 0
        for pair in d:
            if(pair[0].match(fun) != None):
                ok = ok + 1
                if(ok == 1):
                    lam = lambda dd , data = fun , f = pair[1] : f(dd , data)
                    funlambda.append(lam) 
                elif(ok > 1):
                    raise ValueError("Wrong syntax for token: " + fun + " .") 
        if(ok == 0 and fun == "|"):
            ok = ok + 1
            lam = lambda dd , data = funlambda , f = orOper : f(dd , data)
            funlambda = [lam] 
        elif(ok == 0 and fun == "&"):
            ok = ok + 1
            lam = lambda dd , data = funlambda , f = andOper: f(dd , data)
            funlambda = [lam] 
        elif(ok == 0 and fun == "!"):
            ok = ok + 1
            lam = lambda dd , data = funlambda , f = notOper: f(dd , data)
            funlambda = funlambda[:-1] + [lam] 
        if(ok != 1):
            raise ValueError("Wrong syntax in time specification for token: " + fun + " .") 
    if(len(funlambda) == 1):
        return funlambda[0]
    else:
        raise ValueError("Time function stack contains more then one element.")
        

