import datetime
import os
import uuid

import noteparse

class notedata:
    def add(self , keys , note = ""):
        for k in keys:
            if(not k.strip() in self.all_keys):
                raise ValueError("Key description not found. New keys must be added to all_keys.")
        direc = str(uuid.uuid4())
        os.mkdir(os.path.join(self.my_dir , direc))
        os.mkdir(os.path.join(self.my_dir , direc , ".notedata"))
        with open(os.path.join(self.my_dir , direc , ".notedata" , "keys") , "w") as f:
            for k in keys:
                f.write(k.strip() + "\n")
        with open(os.path.join(self.my_dir , direc , ".notedata" , "date") , "w") as f:
            tdy = datetime.date.today()
            f.write(str(tdy.day) + " " + str(tdy.month) + " " + str(tdy.year) + "\n")
        with open(os.path.join(self.my_dir , direc , ".notedata" , "note") , "w") as f:
            f.write(note)
            
    def __init__(self , directory):
        if(not os.path.isdir(directory)): 
            raise ValueError("Directory " + directory + " for notedata does not exist.")

        self.my_dir = directory
        self.all_keys = {}
        self.my_notes = {}

        dirs = os.listdir(directory)
        for d in dirs:
            if(not os.path.isfile(os.path.join(directory , "all_keys"))):
                raise ValueError("File in note directory.")
            with open(os.path.join(directory , "all_keys") , "r") as f:
                for line in f.readlines():
                    if(line.strip() != ""):
                        kd = map(lambda x : x.strip() , line.split("="))
                        kd = list(kd)
                        self.all_keys.update({kd[0] : kd[1]})

            if(not d.strip() == "all_keys"):
                if(not os.path.isdir(os.path.join(directory , d , ".notedata"))):
                    raise ValueError("Note directory does not contain .notedata.")
                keys = []
                with open(os.path.join(directory , d , ".notedata" , "keys") , "r") as f:
                    for line in f.readlines():
                        if(not (line.strip() in self.all_keys)):
                            raise ValueError("Unknown key in note.")
                        keys.append(line.strip())
                date = None
                with open(os.path.join(directory , d , ".notedata" , "date") , "r") as f:
                    dt = list(map(lambda x : int(x) , f.read().strip().split()))
                    date = datetime.date(dt[2] , dt[1] , dt[0])
                nte = None
                with open(os.path.join(directory , d , ".notedata" , "note") , "r") as f:
                    nte = f.read()
                self.my_notes.update({d : [keys , date , nte]})

if(__name__ == "__main__"):
    this_path = os.path.dirname(os.path.realpath(__file__)) 
    nts = notedata(os.path.join(this_path , "test"))
    print(nts.my_notes)
    nts.add(["teaching"] , note = "Some note.")
